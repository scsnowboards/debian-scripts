#!/bin/bash

echo "Installing Spotify"
echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys D2C19886 &&
sudo apt-get update -qq &&
sudo apt-get install libssl1.0.0:amd64 spotify-client -y

echo ''
echo 'You may get an error that you need libssl1.0.0.  If you cannot apt-get it, then you can head to https://packages.debian.org/jessie/amd64/libssl1.0.0/download to download and install'
